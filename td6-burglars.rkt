#lang racket/base

(require "ticker/ticker.rkt"
         "burglars/blog.rkt"
         "burglars/comm-server.rkt"
         "burglars/comm-client.rkt"
         "burglars/world-gen.rkt"
         "burglars/actors-defs.rkt"
         "rlgrid/vec2.rkt"
         "world/wmap-env-functions.rkt"
         "world/wmap-struct.rkt")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Server

(with-blog
  (with-wmap (make-wmap (gen-world-map)) comm-server-broadcast
    (define ticker (run-ticker #:initial-wait #t #:cleanup current-wmap:remove-owned-actor))
    (ticker 'spawn (door (vec2 40 5)))
    (ticker 'spawn (sokobox (vec2 35 5)))
    (ticker 'spawn (sokomark (vec2 35 10)))
    (ticker 'spawn (teleport (vec2 35 15) (vec2 55 5)))
    (for ((_ (in-range 10)))
      (ticker 'spawn (coin (current-wmap:find-free-spot))))
    (comm-server-run comm-client ticker)))
