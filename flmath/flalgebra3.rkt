#lang racket/base

(require "../misc/flonum.rkt"
         racket/performance-hint)

(provide flvec3
         flvec3-dot
         flvec3-length
         flvec3-length^2
         flvec3-dist
         flvec3-dist^2
         flvec3-mul
         flvec3-div
         flvec3-normalize
         flvec3-values
         flvec3-linear
         flvec3-add
         flvec3-neg
         flvec3-sub
         flvec3-cross)

(begin-encourage-inline

  (define (flvec3 (a 0.0) (b 0.0) (c 0.0))
    (flvector a b c))

  (define (flvec3-dot v1 v2)
    (for/fold ((dot 0.0))
              ((c1 (in-flvector v1))
               (c2 (in-flvector v2))
               (n (in-range 3)))
      (fl+ dot (fl* c1 c2))))

  (define (flvec3-length v)
    (flsqrt
     (flvec3-length^2 v)))

  (define (flvec3-length^2 v)
    (flvec3-dot v v))

  (define (flvec3-dist A B)
    (flsqrt (flvec3-dist^2 A B)))

  (define (flvec3-dist^2 A B)
    (define D (flvec3-sub B A))
    (flvec3-length^2 D))

  (define (flvec3-mul v n)
    (for/flvector #:length 3
                  ((c (in-flvector v))
                   (i (in-range 3)))
      (fl* c n)))

  (define (flvec3-div v n)
    (for/flvector #:length 3
                  ((c (in-flvector v))
                   (i (in-range 3)))
      (fl/ c n)))

  (define (flvec3-normalize v)
    (flvec3-div v (flvec3-length v)))

  (define (flvec3-values v)
    (values (flvector-ref v 0)
            (flvector-ref v 1)
            (flvector-ref v 2)))

  (define (flvec3-linear A B v)
    (for/flvector #:length 3
                  ((a (in-flvector A))
                   (b (in-flvector B)))
      (fl+ a (fl* (fl- b a) v))))

  (define (flvec3-add v1 v2)
    (for/flvector #:length 3
                  ((c1 (in-flvector v1))
                   (c2 (in-flvector v2)))
      (fl+ c1 c2)))

  (define (flvec3-neg v)
    (for/flvector #:length 3
                  ((c (in-flvector v)))
      (fl- c)))

  (define (flvec3-sub v1 v2)
    (for/flvector #:length 3
                  ((c1 (in-flvector v1))
                   (c2 (in-flvector v2)))
      (fl- c1 c2)))

  (define (flvec3-cross a b)
    (define-values (ax ay az) (flvec3-values a))
    (define-values (bx by bz) (flvec3-values b))
    (flvector (fl- (fl* ay bz) (fl* az by))
              (fl- (fl* az bx) (fl* ax bz))
              (fl- (fl* ax by) (fl* ay bx))))

  )

(module+ test

  (require rackunit
           "checks.rkt")

  (test-equal? "Dot product of 3-element vectors"
               (flvec3-dot (flvector 1.0 2.0 3.0)
                           (flvector 3.0 2.0 1.0))
               10.0)

  (test-equal? "3D Dot product of 4-element vectors"
               (flvec3-dot (flvector 1.0 2.0 3.0 20.0)
                           (flvector 3.0 2.0 1.0 40.0))
               10.0)

  (test-case "Linear interpolation"
    (define A (flvec3 1.0 2.0 3.0))
    (define B (flvec3 5.0 6.0 7.0))
    (check-flvec=? (flvec3-linear A B 0.5)
                   (flvec3 3.0 4.0 5.0)))

  (test-case "Addition"
    (define A (flvec3 1.0 2.0 3.0))
    (define B (flvec3 5.0 6.0 7.0))
    (check-flvec=? (flvec3-add A B)
                   (flvec3 6.0 8.0 10.0)))

  (test-case "Negation"
    (define A (flvec3 1.0 2.0 3.0))
    (check-flvec=? (flvec3-neg A)
                   (flvec3 -1.0 -2.0 -3.0)))
    
  (test-case "Subtraction"
    (define A (flvec3 1.0 2.0 3.0))
    (define B (flvec3 5.0 6.0 7.0))
    (check-flvec=? (flvec3-sub A B)
                   (flvec3 -4.0 -4.0 -4.0)))

  (test-case "Cross product"
    (define A (flvec3 1.0 0.0 0.0))
    (define B (flvec3 0.0 1.0 0.0))
    (check-flvec=? (flvec3-cross A B)
                   (flvec3 0.0 0.0 1.0)))

  (test-case "Cross product 2"
    (define A (flvec3 0.0 1.0 0.0))
    (define B (flvec3 0.0 0.0 1.0))
    (check-flvec=? (flvec3-cross A B)
                   (flvec3 1.0 0.0 0.0)))

  (test-case "Cross product 3"
    (define A (flvec3 0.0 0.0 1.0))
    (define B (flvec3 1.0 0.0 0.0))
    (check-flvec=? (flvec3-cross A B)
                   (flvec3 0.0 1.0 0.0)))

  )
