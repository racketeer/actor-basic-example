#lang racket/base

(provide wmap:update-actor
         wmap:blocking?
         wmap:opaque?
         wmap:wtile:opaque?
         wmap:send
         wmap:remove-owned-actor
         wmap:find-free-spot
         )

(require "wactor-struct.rkt"
         "wmap-struct.rkt"
         racket/set
         "wtile-struct.rkt"
         "../ticker/ticker-queue.rkt"
         racket/list
         "../rlgrid/generics.rkt"
         "../rlgrid/vec2.rkt"
         )

;; Returns two values - new wmap and new wactor
(define (wmap:update-actor wm wa)
  (cond ((wactor-updated? wa)
         (define nwa (wactor:clean-updated wa))
         (define new-wm (wmap:update-actor* wm
                                            (wactor-id wa)
                                            (wactor-mpos wa)
                                            (wactor-pos wa)
                                            nwa))
         (values new-wm nwa))
        (else
         ;; Nothing changed
         (values wm wa))))

;; Returns true if given tile is blocking movement
(define (wmap:blocking? wm pos)
  (define wt (wmap:ref-tile wm pos))
  (define actors (wmap-actors wm))
  (or (wtile-blocking? wt)
      (for/or ((id (in-set (wtile-actors wt))))
        (wactor-blocking? (hash-ref actors id)))))

;; Returns true if given tile is blocking sight
(define (wmap:opaque? wm pos)
  (define wt (wmap:ref-tile wm pos))
  (define actors (wmap-actors wm))
  (or (wtile-opaque? wt)
      (for/or ((id (in-set (wtile-actors wt))))
        (wactor-opaque? (hash-ref actors id)))))

;; Special form to be used in in-vfan
(define ((wmap:wtile:opaque? wm) wt)
  (define actors (wmap-actors wm))
  (or (wtile-opaque? wt)
      (for/or ((id (in-set (wtile-actors wt))))
        (wactor-opaque? (hash-ref actors id)))))

;; Sends a message to all actors on given tile
(define (wmap:send wm pos msg)
  (define wt (wmap:ref-tile wm pos))
  (define actors (wmap-actors wm))
  (for ((id (in-set (wtile-actors wt))))
    (define actor (hash-ref actors id))
    (define cmd (wactor-cmd actor))
    (when cmd
      (tiqueue-send! cmd msg))))


;; Should be called when owner evaluation finishes
(define (wmap:remove-owned-actor wm ow)
  (define owners (wmap-owners wm))
  (define id (hash-ref owners ow #f))
  (cond (id
         (define-values (nwm nwa)
           (wmap:update-actor wm
                              (wactor:set-pos
                               (hash-ref (wmap-actors wm) id) #f)))
         (struct-copy wmap nwm
                      (owners (hash-remove owners ow))))
        (else
         wm)))

;; Returns vec2 of a non-blocking spot
(define (wmap:find-free-spot wm)
  (car
   (shuffle
    (for/list (((x y) (in-rlgrid #:only-xy (wmap-grid wm)))
               #:when (not (wmap:blocking? wm (vec2 x y))))
      (vec2 x y)))))
