#lang racket/base

(require ffi/unsafe
         ffi/unsafe/define
         "ffi-struct-size.rkt"
         racket/function)

(provide
 termios?
 termios-get
 termios-load
 termios-copy
 termios-make-raw!)

; Get FFI definer for linked libc
(define-ffi-definer define-c (ffi-lib #f))

; int tcgetattr(int fd, struct termios *termios_p);
(define-c tcgetattr (_fun _int _pointer -> _int))

; int tcsetattr(int fd, int optional_actions,
;               const struct termios *termios_p);
(define-c tcsetattr (_fun _int _int _pointer -> _int))

; void cfmakeraw(struct termios *termios_p);
(define-c cfmakeraw (_fun _pointer -> _void))

; Returns struct termios size. Caches result.
(define termios-struct-size
  (cached-ffi-detect-struct-size
   (list (curry tcgetattr 0)
         cfmakeraw)))

; Used for improved contracts
(struct termios (termios))

; Allocates memory for struct termios.
(define (termios-alloc)
  (termios (malloc 'atomic (termios-struct-size))))

; Stores current stdin struct termios to given pre-allocated buffer.
(define (termios-save! termios)
  (tcgetattr 0 (termios-termios termios)))

; Shorthand for getting current struct termios with memory allocation.
(define (termios-get)
  (define tios (termios-alloc))
  (termios-save! tios)
  tios)

; Loads struct termios on stdin from given buffer.
(define (termios-load termios)
  (tcsetattr 0 0 (termios-termios termios))
  (void))

; Creates copy of given struct termios.
(define (termios-copy termios)
  (define copy (termios-alloc))
  (memcpy (termios-termios copy)
          (termios-termios termios)
          (termios-struct-size))
  copy)

; Sets given struct termios up to make it a raw terminal device.
(define (termios-make-raw! termios)
  (cfmakeraw (termios-termios termios))
  (void))
