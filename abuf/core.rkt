#lang racket/base

(require racket/match
         "acell.rkt"
         racket/fixnum)

(provide (struct-out abuf)
         make-abuf
         make-sub-abuf)

(struct abuf
  (width height cells offset stride buffer)
  #:methods gen:custom-write
  ((define (write-proc v port mode)
     (match-define (abuf width height _ offset stride _) v)
     (display (format "#<abuf ~ax~a +~a(~a)>"
                      width height
                      offset stride)
              port))))

(define (make-abuf width height)
  (define size (fx* width height))
  (abuf width height
        (for/vector #:length size
                    ((i (in-range size)))
          (acell #\space #x7f7f7f 0 #t))
        0 width
        (make-bytes (fx+ (fx* size ; Cells
                              ; ESC[38 ; 2 ;255;255;255;48 ; 2 ;255;255;255;utf-8 char
                              (fx+ 1 1 2 1 1 1 3 1 3 1 3 1 2 1 1 1 3 1 3 1 3 1 4))
                         ; Line ends: ESC [ 0 m \r \n
                         (fx* height 6)))))

(define (make-sub-abuf ab x y w h)
  (match-define (abuf width height cells offset stride buffer) ab)
  (when (or (fx< x 0)
            (fx< y 0)
            (fx< w 1)
            (fx< h 1)
            (fx> (fx+ x w) width)
            (fx> (fx+ y h) height))
    (error 'make-sub-abuf
           "Invalid sub-abuf specification ~ax~a+~a+~a cannot fit inside ~ax~a"
           w h x y
           width height))
  (abuf w h
        cells
        (fx+ offset (fx* y stride) x)
        stride
        buffer))

(module+ test

  (require rackunit
           racket/function)

  (test-equal? "Textual description"
               (format "~a" (abuf 10 20 #f 1 16 #f))
               "#<abuf 10x20 +1(16)>")

  (test-case "Construction"
    (define ab (make-abuf 10 10))
    (check-equal? (format "~a" ab)
                  "#<abuf 10x10 +0(10)>"))

  (test-case "Sub abuf construction"
    (define ab (make-abuf 10 10))
    (define sab (make-sub-abuf ab 1 1 8 8))
    (check-equal? (format "~a" sab)
                  "#<abuf 8x8 +11(10)>"))

  (test-case "Sub abuf construction error"
    (define ab (make-abuf 10 10))
    (check-exn exn? (thunk (make-sub-abuf ab 1 1 18 8))))

  )
