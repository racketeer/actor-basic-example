#lang racket/base

(require "../color/core.rkt"
         "../color/blending.rkt"
         racket/list
         "../misc/fixnum.rkt"
         "../misc/vector.rkt"
         racket/performance-hint)

(provide quad-find
         slc-find)

; Bits:
; 0 1
; 2 3
(define quad-lookup
  (vector #\space   ; 00 00
          #\u2598   ; 00 01
          #\u259d   ; 00 10
          #\u2580   ; 00 11
          #\u2596   ; 01 00
          #\u258c   ; 01 01
          #\u259e   ; 01 10
          #\u259b   ; 01 11
          #\u2597   ; 10 00
          #\u259a   ; 10 01
          #\u2590   ; 10 10
          #\u2599   ; 10 11
          #\u2584   ; 11 00
          #\u259c   ; 11 01
          #\u259f   ; 11 10
          #\u2588)) ; 11 11

; List of partitions: fg char, fg partition, bg partition
; Partition: size, list of indices 1-3 (for quads) and spliced list of pairs (0, 1 or 3 for quads)
; The ordering makes 0 match always background!
(define quad-partitions
  '((#\u259f ; #b1110
     (3 (1 2 3) (1 . 2) (1 . 3) (2 . 3))
     (1 (0)))
    (#\u259d ; #b0010
     (1 (1))
     (3 (0 2 3) (0 . 2) (0 . 3) (2 . 3)))
    (#\u2596 ; #b0100
     (1 (2))
     (3 (0 1 3) (0 . 1) (0 . 3) (1 . 3)))
    (#\u2597 ; #b1000
     (1 (3))
     (3 (0 1 2) (0 . 1) (0 . 2) (1 . 2)))
    (#\u2584 ; #b1100
     (2 (2 3) (2 . 3))
     (2 (0 1) (0 . 1)))
    (#\u2590 ; #b1010
     (2 (1 3) (1 . 3))
     (2 (0 2) (0 . 2)))
    (#\u259e ; #b0110
     (2 (1 2) (1 . 2))
     (2 (0 3) (0 . 3)))))

(begin-encourage-inline
  
  (define (count-colors123 . cs)
    ; Returns 1, 2 or 3 (for 3 and more colors present)
    (define c1 (car cs))
    (let loop ((c2 #f)
               (cs (cdr cs)))
      (cond ((null? cs)
             (if c2 2 1))
            (else
             (define c (car cs))
             (if (fx= c c1)
                 (loop c2 (cdr cs))
                 (if c2
                     (if (fx= c c2)
                         (loop c2 (cdr cs))
                         3)
                     (loop c
                           (cdr cs))))))))

  (define (extract-2-colors-from-4 c0 c1 c2 c3)
    ; Extracts 2 colors ensuring c0 is the background (second value returned)
    (define bg c0)
    (define fg (if (fx= bg c1)
                   (if (fx= bg c2)
                       c3
                       c2)
                   c1))
    (values fg bg))

  (define (find-quad-from-2-colors fg bg c0 c1 c2 c3)
    ; Create bit representations, we start with background to be TL part
    (define fbits
      (fxior #;(if (fx= c0 fg) 1 0) ; c0 cannot be fg
             (if (fx= c1 fg) 2 0)
             (if (fx= c2 fg) 4 0)
             (if (fx= c3 fg) 8 0)))

    ; Flipping not useful as if it is problematic there will be bg->fg
    ; transition in top half anyway
    (values (vector-ref quad-lookup fbits)
            fg bg))

  (define (partition-total-distance part . cs)
    ; Partition: (size (indices) (dot pair) ...)
    ; Works for quads and sextants as well
    (if (eq? (car part) 1)
        0 ; Single color
        (for/sum ((cpair (in-list (cddr part))))
          (rgb-distance^2 (list-ref cs (car cpair))
                          (list-ref cs (cdr cpair))))))

  (define (partition-total-distance-v part csv)
    ; Partition: (size (indices) (dot pair) ...)
    ; Works for quads and sextants as well
    (if (eq? (car part) 1)
        0 ; Single color
        (for/sum ((cpair (in-list (cddr part))))
          (rgb-distance^2 (vector-ref csv (car cpair))
                          (vector-ref csv (cdr cpair))))))

  (define (find-best-partitions parts . cs)
    (define csv (apply vector cs))
    (for/fold ((best-partitions #f)
               (best-total-distance 0) ; valid only if best-partition non-#f
               #:result best-partitions)
              ((part (in-list parts)))
      (define part-A (cadr part))
      (define part-B (caddr part))
      ; TODO: apply might have performance impact? Check.
      (define dist-A (partition-total-distance-v part-A csv))
      (define dist-B (partition-total-distance-v part-B csv))
      (define total-distance (fx+ dist-A dist-B))
      (if (or (not best-partitions)
              (fx< total-distance best-total-distance))
          (values part total-distance)
          (values best-partitions best-total-distance))))

  (define (partition-color-average part . cs)
    (apply rgb-average
           (for/list ((index (in-list (cadr part))))
             (list-ref cs index))))

  (define (reduce-quad-colors-to-2 c0 c1 c2 c3)
    (define best-partitions (find-best-partitions quad-partitions c0 c1 c2 c3))
    (define fg (partition-color-average (cadr best-partitions) c0 c1 c2 c3))
    (define bg (partition-color-average (caddr best-partitions) c0 c1 c2 c3))
    (define ch (car best-partitions))
    (values ch fg bg))

  )

(define (quad-find c0 c1 c2 c3)
  ; returns char, fg, bg
  (define n (count-colors123 c0 c1 c2 c3))
  (case n
    ((1) (values #\space c0 c0))
    ((2)
     ; find matching partitions, compute char, use fg preferably for bottom pixels
     (define-values (fg0 bg0) (extract-2-colors-from-4 c0 c1 c2 c3))
     (define-values (ch fg bg) (find-quad-from-2-colors fg0 bg0 c0 c1 c2 c3))
     (values ch fg bg))
    ((3)
     ; reduce palette to two, use the same as in (2)
     (define-values (ch fg bg) (reduce-quad-colors-to-2 c0 c1 c2 c3))
     (values ch fg bg))))

; Bits:
; 0 1
; 2 3
; 4 5
(define slc-lookup
  ;                 ; 65 43 21 (sextant numbering from unicode specification)
  (vector #\space   ; 00 00 00 ; ASCII
          #\U1fb00  ; 00 00 01
          #\U1fb01  ; 00 00 10
          #\U1fb02  ; 00 00 11

          #\U1fb03  ; 00 01 00
          #\U1fb04  ; 00 01 01
          #\U1fb05  ; 00 01 10
          #\U1fb06  ; 00 01 11

          #\U1fb07  ; 00 10 00
          #\U1fb08  ; 00 10 01
          #\U1fb09  ; 00 10 10
          #\U1fb0a  ; 00 10 11

          #\U1fb0b  ; 00 11 00
          #\U1fb0c  ; 00 11 01
          #\U1fb0d  ; 00 11 10
          #\U1fb0e  ; 00 11 11

          #\U1fb0f  ; 01 00 00
          #\U1fb10  ; 01 00 01
          #\U1fb11  ; 01 00 10
          #\U1fb12  ; 01 00 11

          #\U1fb13  ; 01 01 00
          #\u258c   ; 01 01 01 ; Block
          #\U1fb14  ; 01 01 10
          #\U1fb15  ; 01 01 11

          #\U1fb16  ; 01 10 00
          #\U1fb17  ; 01 10 01
          #\U1fb18  ; 01 10 10
          #\U1fb19  ; 01 10 11

          #\U1fb1a  ; 01 11 00
          #\U1fb1b  ; 01 11 01
          #\U1fb1c  ; 01 11 10
          #\U1fb1d  ; 01 11 11

          #\U1fb1e  ; 10 00 00
          #\U1fb1f  ; 10 00 01
          #\U1fb20  ; 10 00 10
          #\U1fb21  ; 10 00 11

          #\U1fb22  ; 10 01 00
          #\U1fb23  ; 10 01 01
          #\U1fb24  ; 10 01 10
          #\U1fb25  ; 10 01 11

          #\U1fb26  ; 10 10 00
          #\U1fb27  ; 10 10 01
          #\u2590   ; 10 10 10 ; Block
          #\U1fb28  ; 10 10 11

          #\U1fb29  ; 10 11 00
          #\U1fb2a  ; 10 11 01
          #\U1fb2b  ; 10 11 10
          #\U1fb2c  ; 10 11 11

          #\U1fb2d  ; 11 00 00
          #\U1fb2e  ; 11 00 01
          #\U1fb2f  ; 11 00 10
          #\U1fb30  ; 11 00 11

          #\U1fb31  ; 11 01 00
          #\U1fb32  ; 11 01 01
          #\U1fb33  ; 11 01 10
          #\U1fb34  ; 11 01 11

          #\U1fb35  ; 11 10 00
          #\U1fb36  ; 11 10 01
          #\U1fb37  ; 11 10 10
          #\U1fb38  ; 11 10 11

          #\U1fb39  ; 11 11 00
          #\U1fb3a  ; 11 11 01
          #\U1fb3b  ; 11 11 10
          #\u2588)) ; 11 11 11 ; Block

(define (get-list-of-bits num (ones? #t) (num-bits 6))
  (let loop ((bit 1)
             (num-bits num-bits)
             (result '())
             (bit-num 0))
    (if (fx= num-bits 0)
        (reverse result)
        (loop (fxlshift bit 1)
              (fx- num-bits 1)
              (if (fx= (fxand num bit)
                       (if ones? bit 0))
                  (cons bit-num result)
                  result)
              (fx+ bit-num 1)))))

(define slc-partitions
  (for/list ((idx (in-range 1 32)))
    ; Get bit combination - flip if c0 is set to foreground (1)
    (define num (if (fx= (fxand idx 1) 1)
                    (fxxor idx 63)
                    idx))
    ; Get list of 1s (foreground mask) and 0s (background mask)
    (define fgnums (get-list-of-bits num #t 6))
    (define bgnums (get-list-of-bits num #f 6))
    (define fgcombs (for/list ((comb (in-list (combinations fgnums 2))))
                      (apply cons comb)))
    (define bgcombs (for/list ((comb (in-list (combinations bgnums 2))))
                      (apply cons comb)))
    `(,(vector-ref slc-lookup num)
      (,(length fgnums) ,fgnums ,@fgcombs)
      (,(length bgnums) ,bgnums ,@bgcombs))))

(begin-encourage-inline
  
  (define (extract-2-colors-from-6 c0 c1 c2 c3 c4 c5)
    ; Extracts 2 colors ensuring c0 is the background (second value returned)
    (define bg c0)
    (define fg (if (fx= bg c1)
                   (if (fx= bg c2)
                       (if (fx= bg c3)
                           (if (fx= bg c4)
                               c5
                               c4)
                           c3)
                       c2)
                   c1))
    (values fg bg))

  (define (find-slc-from-2-colors fg bg c0 c1 c2 c3 c4 c5)
    ; Create bit representations, we start with background to be TL part
    (define fbits
      (fxior #;(if (fx= c0 fg) 1 0) ; c0 cannot be fg
             (if (fx= c1 fg) 2 0)
             (if (fx= c2 fg) 4 0)
             (if (fx= c3 fg) 8 0)
             (if (fx= c4 fg) 16 0)
             (if (fx= c5 fg) 32 0)))
    (values (vector-ref slc-lookup fbits)
            fg bg))

  )

(define (slc-find c0 c1 c2 c3 c4 c5)
  (define n (count-colors123 c0 c1 c2 c3 c4 c5))
  (case n
    ((1) (values #\space c0 c0))
    ((2)
     (define-values (fg0 bg0) (extract-2-colors-from-6 c0 c1 c2 c3 c4 c5))
     (define-values (ch fg bg) (find-slc-from-2-colors fg0 bg0 c0 c1 c2 c3 c4 c5))
     (values ch fg bg))
    ((3)
     (define best-partitions (find-best-partitions slc-partitions c0 c1 c2 c3 c4 c5))
     (define fg (partition-color-average (cadr best-partitions) c0 c1 c2 c3 c4 c5))
     (define bg (partition-color-average (caddr best-partitions) c0 c1 c2 c3 c4 c5))
     (define ch (car best-partitions))
     (values ch fg bg))))

(module+ test

  (require rackunit
           racket/function)

  (test-equal? "Count 1 in 4"
               (count-colors123 1 1 1 1)
               1)

  (test-equal? "Count 2 in 4"
               (count-colors123 2 1 1 2)
               2)

  (test-equal? "Count 3 in 4"
               (count-colors123 2 1 3 2)
               3)

  (test-equal? "Count 4 in 4"
               (count-colors123 2 1 3 4)
               3)

  (test-equal? "Extracting 2 from 4"
               (call-with-values (thunk (extract-2-colors-from-4 1 2 1 2)) list)
               '(2 1))

  (test-equal? "Extracting 2 from 4 (last different)"
               (call-with-values (thunk (extract-2-colors-from-4 1 1 1 2)) list)
               '(2 1))

  (test-equal? "Two colors quad (internal routine)"
               (call-with-values (thunk (find-quad-from-2-colors 2 1 1 2 2 1)) list)
               '(#\u259e 2 1))

  (test-equal? "Partition of 1 element total distance"
               (partition-total-distance '(1 (0)) 0 10 20 30)
               0)

  (test-equal? "Partition of 2 elements total distance"
               (partition-total-distance '(2 (0 1) (0 . 1)) 0 10 20 30)
               100)

  (test-equal? "Partition of 3 elements total distance"
               (partition-total-distance '(3 (0 1 2) (0 . 1) (0 . 2) (1 . 2)) 0 10 20 30)
               600)

  (test-equal? "Find best partitioning"
               ; Using constant definitions here for consistency of
               ; the test (those were the first quad partitions used)
               (find-best-partitions '((1 (1 (0)) (3 (1 2 3) (1 . 2) (1 . 3) (2 . 3)))
                                       (2 (1 (1)) (3 (0 2 3) (0 . 2) (0 . 3) (2 . 3)))
                                       (4 (1 (2)) (3 (0 1 3) (0 . 1) (0 . 3) (1 . 3)))
                                       (8 (1 (3)) (3 (0 1 2) (0 . 1) (0 . 2) (1 . 2)))
                                       (3 (2 (0 1) (0 . 1)) (2 (2 3) (2 . 3)))
                                       (5 (2 (0 2) (0 . 2)) (2 (1 3) (1 . 3)))
                                       (7 (2 (0 3) (0 . 3)) (2 (1 2) (1 . 2))))
                                     1 2 3 100)
               '(8 (1 (3)) (3 (0 1 2) (0 . 1) (0 . 2) (1 . 2))))

  (test-equal? "Partition average"
               (partition-color-average '(3 (0 2 3) (0 . 2) (0 . 3) (2 . 3))
                                        0 10 20 30)
               16)

  (test-equal? "Reducing quad colors"
               (call-with-values (thunk (reduce-quad-colors-to-2 1 100 2 120)) list)
               '(#\u2590 110 1))

  (test-equal? "Single color quad"
               (call-with-values (thunk (quad-find 1 1 1 1)) list)
               '(#\space 1 1))

  (test-equal? "Two colors quad"
               (call-with-values (thunk (quad-find 1 2 2 1)) list)
               '(#\u259e 2 1))

  (test-equal? "Count 1 in 6"
               (count-colors123 1 1 1 1 1 1)
               1)

  (test-equal? "Count 2 in 6"
               (count-colors123 2 1 1 2 1 2)
               2)

  (test-equal? "Count 3 in 6"
               (count-colors123 2 1 3 2 3 2)
               3)

  (test-equal? "Count 4 in 6"
               (count-colors123 2 3 3 4 1 2)
               3)

  (test-equal? "Count 5 in 6"
               (count-colors123 2 3 5 4 1 2)
               3)

  (test-equal? "Count 5 in 6"
               (count-colors123 2 3 5 4 1 6)
               3)

  (test-equal? "Single color SLC"
               (call-with-values (thunk (slc-find 1 1 1 1 1 1)) list)
               '(#\space 1 1))

  (test-equal? "Extracting 2 from 6"
               (call-with-values (thunk (extract-2-colors-from-6 1 2 1 2 1 2)) list)
               '(2 1))

  (test-equal? "Extracting 2 from 4 (first different)"
               (call-with-values (thunk (extract-2-colors-from-6 2 1 1 1 1 2)) list)
               '(1 2))

  (test-equal? "Two colors SLC (internal routine)"
               (call-with-values (thunk (find-slc-from-2-colors 2 1 1 2 2 1 1 2)) list)
               '(#\U1fb24 2 1))

  (test-equal? "Two colors SLC"
               (call-with-values (thunk (slc-find 1 2 2 1 1 2)) list)
               '(#\U1fb24 2 1))

  (test-equal? "List of bits - 1s"
               (get-list-of-bits #b011010 #t 6)
               '(1 3 4))

  (test-equal? "List of bits - 0s"
               (get-list-of-bits #b011010 #f 6)
               '(0 2 5))

  )
