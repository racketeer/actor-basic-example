#lang racket/base

(require "fixnum.rkt"
         "flonum.rkt"
         racket/performance-hint)

(provide clip-range
         clip-rect
         double-clip-range
         double-clip-rect
         clip-range/scale)

(begin-encourage-inline

  (define (clip-range x w bx bw)
    (define-values (cx1 cw1)
      (if (fx< x bx)
          (values bx (fx+ w (fx- x bx)))
          (values x w)))
    (define-values (cx2 cw2)
      (if (fx> (fx+ cx1 cw1) bw)
          (values cx1 (- bw cx1))
          (values cx1 cw1)))
    (values cx2 (if (fx< cw2 0) 0 cw2)))

  (define (clip-rect x y w h
                     bx by bw bh)
    (define-values (cx cw) (clip-range x w bx bw))
    (define-values (cy ch) (clip-range y h by bh))
    (values cx cy cw ch))

  (define (double-clip-range sx1 sw1 dx1 dw)
    ; if dx<0 set to 0 increment sx, decrement sw
    (define-values (sx2 sw2 dx2)
      (if (fx< dx1 0)
          (values (fx- sx1 dx1) (fx+ sw1 dx1) 0)
          (values sx1 sw1 dx1)))
    ; if dx2+sw2>dw decrease sw2
    (define-values (sx3 sw3 dx3)
      (if (fx> (fx+ dx2 sw2) dw)
          (values sx2 (fx- dw dx2) dx2)
          (values sx2 sw2 dx2)))
    (values sx3 sw3 dx3 dw))

  (define (double-clip-rect sx sy sw sh
                            dx dy dw dh)
    (define-values (rsx rsw rdx rdw) (double-clip-range sx sw dx dw))
    (define-values (rsy rsh rdy rdh) (double-clip-range sy sh dy dh))
    (values rsx rsy rsw rsh rdx rdy rdw rdh))

  (define (clip-range/scale x w bx bw us ue)
    (define-values (x1 w1 us1 ue1)
      (if (fx< x bx)
          (values bx
                  (fx- w (fx- bx x))
                  ; us+=(bx-x)*uw/w
                  (fl+ us
                       (fl/ (fl* (fx->fl (fx- bx x))
                                 (fl- ue us))
                            (fx->fl w)))
                  ue)
          (values x w us ue)))
    (define-values (x2 w2 us2 ue2)
      (if (fx> (fx+ x1 w1) bw)
          (values x1
                  (fx- bw x1)
                  us1
                  ; ue-=(x1+w-bw)*uw/w
                  (fl- ue
                       (fl/ (fl* (fx->fl (fx- (fx+ x1 w1) bw))
                                 (fl- ue1 us1))
                            (fx->fl w1))))
          (values x1 w1 us1 ue1)))
    (values x1 w2 us2 ue2))

  )

(module+ test

  (require rackunit
           racket/function)

  (test-equal? "clip-range: within"
               (call-with-values
                (thunk
                 (clip-range 1 2 0 10))
                list)
               '(1 2))

  (test-equal? "clip-range: left-clip"
               (call-with-values
                (thunk
                 (clip-range -1 2 0 10))
                list)
               '(0 1))

  (test-equal? "clip-range: right-clip"
               (call-with-values
                (thunk
                 (clip-range 9 2 0 10))
                list)
               '(9 1))

  (test-equal? "clip-range: total-clip"
               (call-with-values
                (thunk
                 (clip-range -9 20 0 10))
                list)
               '(0 10))

  (test-equal? "clip-range: nothing remains"
               (call-with-values
                (thunk
                 (clip-range -9 5 0 10))
                list)
               '(0 0))

  (test-equal? "double-clip-range: within"
               (call-with-values (thunk (double-clip-range 0 10 0 10)) list)
               '(0 10 0 10))

  (test-equal? "double-clip-range: source clip right against destination"
               (call-with-values (thunk (double-clip-range 0 10 5 10)) list)
               '(0 5 5 10))

  (test-equal? "double-clip-range: destination clip left"
               (call-with-values (thunk (double-clip-range 0 10 -5 10)) list)
               '(5 5 0 10))

  (test-equal? "Scaled clip within"
               (call-with-values (thunk (clip-range/scale 10 10 0 30 0.0 1.0)) list)
               '(10 10 0.0 1.0))

  (test-equal? "Scaled clip left"
               (call-with-values (thunk (clip-range/scale -5 10 0 30 0.0 1.0)) list)
               '(0 5 0.5 1.0))

  (test-equal? "Scaled clip right"
               (call-with-values (thunk (clip-range/scale 25 10 0 30 0.0 1.0)) list)
               '(25 5 0.0 0.5))

  )
