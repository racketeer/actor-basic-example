#lang racket/base

(require "generics.rkt"
         racket/function
         racket/list
         "../misc/fixnum.rkt")

(provide dtgrid?
         make-dtgrid
         dtgrid-stats
         rlgrid->dtgrid)

(struct collapsed (value))

(struct default (proc))

(define (node-cons a d)
  (cond ((and (collapsed? a)
              (collapsed? d)
              (equal? (collapsed-value a)
                      (collapsed-value d)))
         ; two collapsed subtrees with the same value
         a)
        ((and (not (pair? a))
              (not (pair? d))
              (not (collapsed? a))
              (not (collapsed? d))
              (equal? a d))
         ; two leaves with the same value
         (collapsed a))
        (else
         (cons a d))))

(define (node-car/cdr node)
  (cond ((collapsed? node)
         (values node node))
        ((default? node)
         (values node node))
        (else
         (values (car node) (cdr node)))))

(struct dtgrid
  (width height root)
  #:methods gen:rlgrid
  ((define (rlgrid-ref g x y (default rlgrid-invalid-tile))
     (define (node-ref node w h nx ny)
       (cond ((collapsed? node)
              (collapsed-value node))
             ((default? node)
              (define proc (default-proc node))
              (if (arity=? (procedure-arity proc) 2)
                  (proc x y)
                  (proc)))
             ((= w h 1)
              node)
             ((fx> h w)
              (define h/2 (fxrshift h 1))
              (if (fx< ny h/2)
                  (node-ref (car node) w h/2 nx ny)
                  (node-ref (cdr node) w (fx- h h/2) nx (fx- ny h/2))))
             (else
              (define w/2 (fxrshift w 1))
              (if (fx< nx w/2)
                  (node-ref (car node) w/2 h nx ny)
                  (node-ref (cdr node) (fx- w w/2) h (fx- nx w/2) ny)))))
     (define width (dtgrid-width g))
     (define height (dtgrid-height g))
     (cond ((or (fx< x 0)
                (fx< y 0)
                (fx>= x width)
                (fx>= y height))
            (if (eq? default rlgrid-invalid-tile)
                (error 'dtgrid-ref "~v,~v out of bounds ~vx~v"
                       x y width height)
                default))
           (else
            (node-ref (dtgrid-root g)
                      width
                      height
                      x y))))
   (define (rlgrid-set g x y v)
     (define (node-set node w h nx ny)
       (cond ((= w h 1)
              v)
             ((fx> h w)
              (define h/2 (fxrshift h 1))
              (define-values (node-car node-cdr) (node-car/cdr node))
              (if (fx< ny h/2)
                  (node-cons (node-set node-car w h/2 nx ny)
                             node-cdr)
                  (node-cons node-car
                             (node-set node-cdr w (fx- h h/2) nx (fx- ny h/2)))))
             (else
              (define w/2 (fxrshift w 1))
              (define-values (node-car node-cdr) (node-car/cdr node))
              (if (fx< nx w/2)
                  (node-cons (node-set node-car w/2 h nx ny)
                             node-cdr)
                  (node-cons node-car
                             (node-set node-cdr (fx- w w/2) h (fx- nx w/2) ny))))))
     (define width (dtgrid-width g))
     (define height (dtgrid-height g))
     (when (or (fx< x 0)
               (fx< y 0)
               (fx>= x width)
               (fx>= y height))
       (error 'dtgrid-sef "~v,~v out of bounds ~vx~v"
              x y width height))
     (struct-copy
      dtgrid g
      (root
       (node-set
        (dtgrid-root g)
        width
        height
        x y))))
   (define (rlgrid-width g)
     (dtgrid-width g))
   (define (rlgrid-height g)
     (dtgrid-height g))
   (define (rlgrid-multi-set g . triplets-flat)
     (define triplets
       (map (curry map cadr)
            (group-by (lambda (l) (fxquotient (car l) 3))
                      (for/list ((t (in-list triplets-flat))
                                 (i (in-naturals)))
                        (list i t)))))
     (define (nodes-set node w h triplets)
       (cond ((= w h 1)
              (when (not (eq? (length triplets) 1))
                (error 'tlgrid-multi-set/nodes-set
                       (format "Exactly one value for each position is needed: ~v"
                               triplets)))
              (define v (caddar triplets))
              v)
             ((fx> h w)
              (define h/2 (fxrshift h 1))
              (define-values (node-car node-cdr) (node-car/cdr node))
              (define-values (triplets-car triplets-cdr)
                (partition (λ (t)
                             (define-values (nx ny v) (apply values t))
                             (fx< ny h/2))
                           triplets))
              (define triplets-cdr-
                (for/list ((t (in-list triplets-cdr)))
                  (define-values (nx ny v) (apply values t))
                  (list nx (fx- ny h/2) v)))
              (node-cons (if (empty? triplets-car)
                             node-car
                             (nodes-set node-car w h/2 triplets-car))
                         (if (empty? triplets-cdr-)
                             node-cdr
                             (nodes-set node-cdr w (fx- h h/2) triplets-cdr-))))
             (else
              (define w/2 (fxrshift w 1))
              (define-values (node-car node-cdr) (node-car/cdr node))
              (define-values (triplets-car triplets-cdr)
                (partition (λ (t)
                             (define-values (nx ny v) (apply values t))
                             (fx< nx w/2))
                           triplets))
              (define triplets-cdr-
                (for/list ((t (in-list triplets-cdr)))
                  (define-values (nx ny v) (apply values t))
                  (list (fx- nx w/2) ny v)))
              (node-cons (if (empty? triplets-car)
                             node-car
                             (nodes-set node-car w/2 h triplets-car))
                         (if (empty? triplets-cdr-)
                             node-cdr
                             (nodes-set node-cdr (fx- w w/2) h triplets-cdr-))))))
     (struct-copy
      dtgrid g
      (root
       (nodes-set
        (dtgrid-root g)
        (dtgrid-width g)
        (dtgrid-height g)
        triplets))))
   ))

(define (make-dtgrid width height (default-value #f) #:force (force #f))
  (cond
    ((or (fx<= width 0)
         (fx<= height 0))
     (error 'make-dtgrid "invalid size ~vx~v" width height))
    ((procedure? default-value)
     (cond (force
            (for*/fold ((g (dtgrid width height (collapsed #f))))
                       ((y (in-range height))
                        (x (in-range width)))
              (rlgrid-set g x y
                          (if (arity=? (procedure-arity default-value) 2)
                              (default-value x y)
                              (default-value)))))
           (else
            (dtgrid width height
                    (default default-value)))))
    (else
     (dtgrid width
             height
             (collapsed default-value)))))

(define (dtgrid-stats tg)
  ; (values cons collapsed default value)
  (define (node-stats node w h)
    (cond ((collapsed? node)
           (values 0 1 0 0))
          ((default? node)
           (values 0 0 1 0))
          ((= w h 1)
           (values 0 0 0 1))
          ((fx> h w)
           (define h/2 (fxrshift h 1))
           (define-values (a0 a1 a2 a3) (node-stats (car node) w h/2))
           (define-values (d0 d1 d2 d3) (node-stats (cdr node) w (fx- h h/2)))
           (values (fx+ a0 d0 1)
                   (fx+ a1 d1)
                   (fx+ a2 d2)
                   (fx+ a3 d3)))
          (else
           (define w/2 (fxrshift w 1))
           (define-values (a0 a1 a2 a3) (node-stats (car node) w/2 h))
           (define-values (d0 d1 d2 d3) (node-stats (cdr node) (fx- w w/2) h))
           (values (fx+ a0 d0 1)
                   (fx+ a1 d1)
                   (fx+ a2 d2)
                   (fx+ a3 d3)))))
  (node-stats (dtgrid-root tg)
              (dtgrid-width tg)
              (dtgrid-height tg)))

(define (rlgrid->dtgrid rlg)
  (if (dtgrid? rlg)
      rlg
      (make-dtgrid (rlgrid-width rlg)
                   (rlgrid-height rlg)
                   (λ (x y)
                     (rlgrid-ref rlg x y)))))

(module+ test
  (require rackunit
;           (submod "..") ; Needed to explicitly require surrounding
;                         ; module provides with attached contracts
           racket/port
           )

  (test-case "Creation and inspection"
    (define tg0 (make-dtgrid 5 6))
    (check-true (dtgrid? tg0))
    (check-eq? (rlgrid-width tg0) 5)
    (check-eq? (rlgrid-height tg0) 6))

  (test-case "Creation exceptions"
    (check-exn exn? (thunk (make-dtgrid 5 0)))
    (check-exn exn? (thunk (make-dtgrid -1 5)))
    (check-exn exn? (thunk (make-dtgrid -10 -15))))

  (test-case "Creation with no-argument procedure"
    (define tg0 (make-dtgrid 1 1 (thunk 'no-argument)))
    (check-eq? (rlgrid-ref tg0 0 0) 'no-argument))
  
  (test-case "Creation with x,y-argument procedure and referencing"
    (define tg0 (make-dtgrid 3 4 (λ (x y) (format "~a,~a" x y))))
    (for* ((y (in-range 4))
           (x (in-range 3)))
      (check-equal? (rlgrid-ref tg0 x y) (format "~a,~a" x y))))

  (test-case "Referencing with out of bounds arguments"
    (define tg0 (make-dtgrid 5 6))
    (check-exn exn? (thunk (rlgrid-ref tg0 -1 0)))
    (check-exn exn? (thunk (rlgrid-ref tg0 -1 -2)))
    (check-exn exn? (thunk (rlgrid-ref tg0 6 3)))
    (check-exn exn? (thunk (rlgrid-ref tg0 2 7)))
    (check-exn exn? (thunk (rlgrid-ref tg0 8 7))))

  (test-case "Setting grid elements"
    (define tg0 (make-dtgrid 5 6))
    (define tg1 (rlgrid-set tg0 0 0 'tg1))
    (check-eq? (rlgrid-ref tg1 0 0) 'tg1)
    (define tg2 (rlgrid-set tg1 4 5 'tg2))
    (check-eq? (rlgrid-ref tg2 4 5) 'tg2)
    (check-eq? (rlgrid-ref tg2 0 0) 'tg1)
    (check-false (rlgrid-ref tg2 3 3)))

  (test-case "Modifying with out of bounds arguments"
    (define tg0 (make-dtgrid 5 6))
    (check-exn exn? (thunk (rlgrid-set tg0 -1 0 1)))
    (check-exn exn? (thunk (rlgrid-set tg0 -1 -2 2)))
    (check-exn exn? (thunk (rlgrid-set tg0 6 3 3)))
    (check-exn exn? (thunk (rlgrid-set tg0 2 7 4)))
    (check-exn exn? (thunk (rlgrid-set tg0 8 7 5))))

  (test-case "Iteration over the whole dtgrid"
    (define tg0 (make-dtgrid 3 2 (λ (x y) (format "~a,~a" x y))))
    (check-equal? (with-output-to-string
                    (thunk
                     (for ((v (in-rlgrid tg0)))
                       (display v))))
                  "0,01,02,00,11,12,1")
    (check-equal? (with-output-to-string
                    (thunk
                     (for (((x y v) (in-rlgrid #:with-xy tg0)))
                       (display (format "~a:~a:~a" x y v)))))
                  "0:0:0,01:0:1,02:0:2,00:1:0,11:1:1,12:1:2,1"))

  (test-case "Iteration over part of dtgrid"
    (define tg0 (make-dtgrid 5 6 (λ (x y) (format "~a,~a" x y))))
    (check-equal? (with-output-to-string
                    (thunk
                     (for ((v (in-rlgrid tg0 1 1 3 2)))
                       (display v))))
                  "1,12,1"))

  (test-case "Partial clip"
    (define tg0 (make-dtgrid 5 6 (λ (x y) (format "~a,~a" x y))))
    (check-equal? (with-output-to-string
                    (thunk
                     (for ((v (in-rlgrid tg0 -2 -10 3 2)))
                       (display v))))
                  "0,01,02,00,11,12,1"))

  (test-case "Partial clip with xy"
    (define tg0 (make-dtgrid 5 6 (λ (x y) (format "~a,~a" x y))))
    (check-equal? (with-output-to-string
                    (thunk
                     (for (((x y v) (in-rlgrid #:with-xy tg0 -2 -10 3 2)))
                       (display v))))
                  "0,01,02,00,11,12,1"))

  (test-case "Full clip with xy"
    (define tg0 (make-dtgrid 3 2 (λ (x y) (format "~a,~a" x y))))
    (check-equal? (with-output-to-string
                    (thunk
                     (for (((x y v) (in-rlgrid #:with-xy tg0 -2 -10 30 20)))
                       (display v))))
                  "0,01,02,00,11,12,1"))

  (test-case "Check allocations"
    (define tg0 (make-dtgrid 4 3))
    (define tg1 (rlgrid-set tg0 0 0 'test))
    (define tg2 (rlgrid-set tg1 1 0 'test))
    (define tg3 (rlgrid-set tg2 1 0 #f))
    (check-equal? (call-with-values (thunk (dtgrid-stats tg1)) list)
                  '(3 3 0 1))
    (check-equal? (call-with-values (thunk (dtgrid-stats tg2)) list)
                  '(2 3 0 0))
    (check-equal? (call-with-values (thunk (dtgrid-stats tg3)) list)
                  '(3 3 0 1))
    )

  (test-case "Multi-set with lazy default"
    (define tg0 (make-dtgrid 3 2 (λ (x y) (format "~a,~a" x y))))
    (define tg1 (rlgrid-multi-set tg0
                                  0 0 "X"
                                  1 1 "Y"
                                  2 1 "Z"))
    (check-equal? (with-output-to-string
                    (thunk
                     (for (((x y v) (in-rlgrid #:with-xy tg1 -2 -10 30 20)))
                       (display v))))
                  "X1,02,00,1YZ"))

  )
